package boot.spring.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import boot.spring.bootstrap.StarterRun;
import boot.spring.pagemodel.AjaxResult;
import boot.spring.pagemodel.DwNode;
import boot.spring.pagemodel.XzqhNode;
import boot.spring.po.Dwb;
import boot.spring.po.Xzqh;
import boot.spring.service.DwService;
import boot.spring.service.XzqhService;
import boot.spring.tools.TreeUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@Api(tags = "树状结构生成接口")
@Controller
public class TreeController {
	
	@Autowired
	XzqhService xzqhService;
	
	@Autowired
    DwService dwService;
	
	@ApiOperation("生成单位树状结构")
	@RequestMapping(value="/DWTree",method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult DWTree(){
		System.out.println("===========初始化单位树开始=========");
        long startTime = System.currentTimeMillis();
        // 查询所有单位
        List<Dwb> dwblist = dwService.listDwb();
        List<DwNode> nodes = new ArrayList<>();
        // 把单位数据转化为节点对象列表
        dwblist.stream().forEach(dw->{
        	DwNode node = dwService.transformDwbToNode(dw);
        	nodes.add(node);
        });
        // 将节点列表转化为节点树，从根节点开始递归构建
        DwNode tree = TreeUtils.buildTree(nodes);
        long endTime = System.currentTimeMillis();
        System.out.println("初始化单位结束,耗时"+(endTime-startTime));
        return AjaxResult.success(tree);
	}
	
	@ApiOperation("生成行政区划树状结构")
	@RequestMapping(value="/XZQHTree",method = RequestMethod.GET)
	@ResponseBody
	public AjaxResult XZQHTree(){
		System.out.println("===========初始化行政区划树开始=========");
        long startTime = System.currentTimeMillis();
        // 查询所有行政区
        List<Xzqh> xzqlist = xzqhService.listXzqh();
        List<XzqhNode> nodes = new ArrayList<>();
        // 把PO数据转化为节点对象列表
        xzqlist.stream().forEach(xzqh->{
        	XzqhNode node = xzqhService.transformXzqhToNode(xzqh);
        	nodes.add(node);
        });
        // 将节点列表转化为节点树，从根节点开始递归构建
        List<XzqhNode> tree = TreeUtils.buildMultiTree(nodes);
        long endTime = System.currentTimeMillis();
        System.out.println("初始化行政区划树结束,耗时"+(endTime-startTime));
		return AjaxResult.success(tree);
	}
	
}
