package boot.spring.controller;

import javax.servlet.http.HttpSession;

import boot.spring.pagemodel.AjaxResult;
import boot.spring.po.Staff;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import boot.spring.service.LoginService;
import boot.spring.tools.JwtTokenUtils;



@Controller
public class LoginController {
    @Autowired
    LoginService loginservice;

    @Autowired
    JwtTokenUtils jwtTokenUtils;

    @RequestMapping(value="/loginvalidate",method=RequestMethod.POST)
    @ResponseBody
    public AjaxResult loginvalidate(@RequestParam("username") String username,@RequestParam("password") String pwd) throws Exception{
        Staff user=loginservice.getpwdbyname(username);
        if(user!=null&&pwd.equals(user.getPassword()))
        {
            // 登录成功，创建token
            String token = jwtTokenUtils.createToken(user.getStaff_id(), "user");
            user.setToken(token);
            return AjaxResult.success(user);
        } else {
            return AjaxResult.error("登录失败");
        }
    }


    @RequestMapping(value="/logout",method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult logout(){
        jwtTokenUtils.deleteToken();
        return AjaxResult.success("退出成功");
    }

    @RequestMapping(value="/login",method = RequestMethod.GET)
    String login(){
        return "login";
    }

    @RequestMapping(value="/currentuser",method = RequestMethod.GET)
    @ResponseBody
    public AjaxResult curruntuser(){
        Long userid = JwtTokenUtils.getUserId();
        Staff s = loginservice.getById(userid);
        s.setToken(JwtTokenUtils.getRequestToken());
        return AjaxResult.success(s);
    }

}
