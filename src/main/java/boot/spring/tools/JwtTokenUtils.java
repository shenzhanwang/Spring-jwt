package boot.spring.tools;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.interfaces.Claim;
import com.auth0.jwt.interfaces.DecodedJWT;

import boot.spring.exception.AuthorizationException;

import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * JWT工具类
 */
@Component
public class JwtTokenUtils {


    /** token秘钥，请勿泄露，请勿随便修改*/
    public static final String SECRET = "SPRINGJWTSALT1014967";
    /** token 过期时间: 7天 */
    public static final int calendarField = Calendar.DATE;
    public static final int calendarInterval = 7;

    @Autowired
    RedissonClient redissonClient;

    /**
     * JWT生成Token.
     *
     * JWT构成: header, payload, signature
     *
     * @param userId
     * 登录成功后用户userId, 参数userId不可传空
     */
    public String createToken(Long userId,String aud) throws Exception {
        Date iatDate = new Date();
        // expire time
        Calendar nowTime = Calendar.getInstance();
        nowTime.add(calendarField, calendarInterval);
        Date expiresDate = nowTime.getTime();

        // header Map
        Map<String, Object> map = new HashMap<>();
        map.put("alg", "HS256");
        map.put("typ", "JWT");

        // build token
        // param backups {iss:Service, aud:APP}
        String token = JWT.create().withHeader(map) // header
                .withClaim("iss", "Service") // payload
                .withClaim("aud", aud)
                .withClaim("userId", null == userId ? null : userId.toString())
                .withClaim("timestamp", LocalDateTime.now().toInstant(ZoneOffset.of("+8")).toEpochMilli())
                .withClaim("sequences", RandomStringUtils.random(4))
                .withIssuedAt(iatDate) // sign time
                .withExpiresAt(expiresDate) // expire time
                .sign(Algorithm.HMAC256(SECRET)); // signature
        RBucket<String> stringdata = redissonClient.getBucket("user-token-" + userId);
        stringdata.set(token, 7, TimeUnit.DAYS);
        return token;
    }

    /**
     * 解密Token
     *
     * @param token
     * @return
     * @throws Exception
     */
    public static Map<String, Claim> verifyToken(String token) {
        DecodedJWT jwt = null;
        try {
            JWTVerifier verifier = JWT.require(Algorithm.HMAC256(SECRET)).build();
            jwt = verifier.verify(token);
        } catch (Exception e) {
            // token 校验失败, 抛出Token验证非法异常
            throw new AuthorizationException("Token验证非法");
        }
        return jwt.getClaims();
    }

    /**
     * 获取当前userId
     *
     * @return userId
     */
    public static Long getUserId() {
        String token = getRequestToken();
        Map<String, Claim> claims = verifyToken(token);
        Claim userIdClaim = claims.get("userId");
        if (null == userIdClaim || StringUtils.isEmpty(userIdClaim.asString())) {
            // token 校验失败, 抛出Token验证非法异常
            throw new AuthorizationException("登陆异常，请重新登录");
        }
        return Long.valueOf(userIdClaim.asString());
    }

    /**
     * 获取请求的token
     */
    public static String getRequestToken() {
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //从header中获取token
        String token = request.getHeader("Authorization");

        //如果header中不存在token，则从参数中获取token
        if (org.springframework.util.StringUtils.isEmpty(token)) {
            token = request.getParameter("Authorization");
        }

        return token;
    }

    public void deleteToken() {
        Long userId = getUserId();
        RBucket<String> stringdata = redissonClient.getBucket("user-token-" + userId);
        stringdata.delete();
    }
}
