package boot.spring.exception;

public class AuthorizationException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    protected final String message;

    public AuthorizationException(String message)
    {
        this.message = message;
    }

    @Override
    public String getMessage()
    {
        return message;
    }
}
