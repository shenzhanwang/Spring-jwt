package boot.spring.service;

import boot.spring.po.Staff;

public interface LoginService {
	Staff getpwdbyname(String name);

	Staff getById(Long id);
}
