package boot.spring.po;


public class Staff {
	private Long staff_id;
	private String first_name;
	private String last_name;
	private Short address_id;
	private String email;
	private String username;
	private String password;
	private String last_update;
	private String token;

	public Long getStaff_id() {
		return staff_id;
	}

	public void setStaff_id(Long staff_id) {
		this.staff_id = staff_id;
	}




	public String getFirst_name() {
		return first_name;
	}




	public void setFirst_name(String first_name) {
		this.first_name = first_name;
	}




	public String getLast_name() {
		return last_name;
	}




	public void setLast_name(String last_name) {
		this.last_name = last_name;
	}




	public Short getAddress_id() {
		return address_id;
	}




	public void setAddress_id(Short address_id) {
		this.address_id = address_id;
	}




	public String getEmail() {
		return email;
	}




	public void setEmail(String email) {
		this.email = email;
	}




	public String getUsername() {
		return username;
	}




	public void setUsername(String username) {
		this.username = username;
	}




	public String getPassword() {
		return password;
	}




	public void setPassword(String password) {
		this.password = password;
	}




	public String getLast_update() {
		return last_update;
	}




	public void setLast_update(String last_update) {
		this.last_update = last_update;
	}




	public String getToken() {
		return token;
	}




	public void setToken(String token) {
		this.token = token;
	}




	@Override
	public String toString() {
		return "Staff [staff_id=" + staff_id + ", first_name=" + first_name + ", last_name=" + last_name
				+ ", address_id=" + address_id + ", email=" + email + ", username=" + username + ", password="
				+ password + ", last_update=" + last_update + ", token=" + token + "]";
	}

}