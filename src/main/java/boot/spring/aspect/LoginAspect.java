package boot.spring.aspect;


import org.apache.commons.lang3.StringUtils;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import boot.spring.exception.AuthorizationException;
import boot.spring.pagemodel.AjaxResult;

/**
 * 登录校验的切面，若未登录则重定向到登录页面
 * @author shenzhanwang
 */
@Component
@Aspect
public class LoginAspect {

    @Autowired
    private LoginCheckModel loginCheckModel;

    @Pointcut("execution(* boot.spring.controller.*.*(..)) && "
            + "!execution(* boot.spring.controller.LoginController.*(..))") // Login中写了登录方法
    public void login() {
    }


    @Around("login()")
    public Object auth(ProceedingJoinPoint joinPoint) throws Throwable {
        String token = loginCheckModel.getToken();
        if (StringUtils.isBlank(token)) {
            throw new AuthorizationException("您还未登录，请先登录");
        }
        if (loginCheckModel.check(token, "user")) {
            return joinPoint.proceed();
        } else {
            throw new AuthorizationException("登录异常，请重新登录");
        }
    }
}
