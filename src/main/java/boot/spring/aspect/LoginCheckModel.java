package boot.spring.aspect;

import com.auth0.jwt.interfaces.Claim;

import boot.spring.exception.AuthorizationException;
import boot.spring.tools.JwtTokenUtils;

import org.apache.commons.lang3.StringUtils;
import org.redisson.api.RBucket;
import org.redisson.api.RedissonClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

/**
 */
@Component
public class LoginCheckModel {

    @Autowired
    RedissonClient redissonClient;

    public String getToken() {
        //获取token
        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        HttpServletRequest request = attributes.getRequest();
        //从header中获取token
        String token = request.getHeader("Authorization");
        //如果header中不存在token，则从参数中获取token
        if (org.springframework.util.StringUtils.isEmpty(token)) {
            token = request.getParameter("Authorization");
        }
        return token;
    }

    public boolean check(String token, String checkAud) {
        Map<String, Claim> stringClaimMap = JwtTokenUtils.verifyToken(token);
        Claim aud = stringClaimMap.get("aud");
        if (StringUtils.isNotBlank(checkAud)) {
            if (!aud.asString().equals(checkAud)) {
                throw new AuthorizationException("登录异常，请重新登录");
            }
        }
        Claim exp = stringClaimMap.get("exp");
        Date date = exp.asDate();
        if (date.before(new Date())) {
            throw new AuthorizationException("登录已过期，请重新登录");
        }
        Claim user = stringClaimMap.get("userId");
        Long userId = Long.parseLong(user.asString());
        // 去redis验证token
        RBucket<String> stringdata = redissonClient.getBucket("user-token-" + userId);
        String cacheToken = stringdata.get();
        if(StringUtils.isBlank(cacheToken)){
            throw new AuthorizationException("您还未登录，请登录");
        }
        if (!token.equals(cacheToken)) {
            throw new AuthorizationException("您已在别的地方登录，请重新登录");
        }
        return true;
    }

}
